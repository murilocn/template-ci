from sklearn.datasets import make_blobs
import pandas as pd
import hdbscan
import pickle


print("Criando conjunto de dados...\n")

blobs, labels = make_blobs(n_samples=100000, n_features=10)

pd.DataFrame(blobs).head()

print("Inicializando HDBSCAN...\n")

clusterer = hdbscan.HDBSCAN(algorithm='best', alpha=1.0, approx_min_span_tree=True, gen_min_span_tree=False, leaf_size=40, metric='euclidean', min_cluster_size=5, min_samples=None, p=None)

result = clusterer.fit(blobs)

print("Salvando resultados...\n")

pickle.dump(result, open( "result.p", "wb" ))

print("Feito!\n")




